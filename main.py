#!/usr/bin/python

import curses,curses.wrapper
import random

# standard screen (curses funcs)
s=None

random_nicks=["hotbabe","elite","hacker","aegis"]
# key layout stuff
choicekeys="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
directionkeys="kulnjbhy" # in same order as the direction constants below

# directions
DIR_N=0
DIR_NE=1
DIR_E=2
DIR_SE=3
DIR_S=4
DIR_SW=5
DIR_W=6
DIR_NW=7

def toptext(info):
	s.move(0,0)
	s.clrtoeol()
	s.addstr(0,0,info)

def happening(info):
	"Write these messages to a history listing"
	toptext(info)

def biginfo(info):
	s.clear()
	s.move(0,0)
	s.addstr(0,0,info)
	key_to_continue()

def key_to_continue():
	s.addstr("...")
	s.getch()

def choice_menu(choices):
	i=1
	for c in choices:
		s.addstr(i,2,"[{0}] {1}".format(choicekeys[i-1],c))
		i+=1
	sel=s.getkey()
	while sel not in choicekeys[0:len(choices)]:
		sel=s.getkey()
	return choices[choicekeys.find(sel)]

def random_nick():
	return "{0}{1}".format(random.choice(random_nicks), random.randint(1,999))

def text_prompt(prompt):
	toptext("{0}: ".format(prompt))
	curses.echo()
	sel=s.getstr()
	curses.noecho()
	return sel

class Character:
	def __init__(self,nick=random_nick()):
		self.x=10
		self.y=10
		self.nick=nick
	def draw(self,letter='?'):
		s.addch(self.y,self.x,letter)
	def think(self):
		pass
	def move(self,direction):
		if direction in [DIR_NW,DIR_N,DIR_NE]: self.y-=1
		if direction in [DIR_SW,DIR_S,DIR_SE]: self.y+=1
		if direction in [DIR_NW,DIR_W,DIR_SW]: self.x-=1
		if direction in [DIR_NE,DIR_E,DIR_SE]: self.x+=1

class Player(Character):
	def draw(self):
		Character.draw(self,'@')
	def think(self):
		sel=s.getkey()
		if sel in directionkeys:
			self.move(directionkeys.find(sel))

class Tile():
	def __init__(self,character=" "):
		self.character=character
	def draw(self,x,y):
		s.addch(y,x,self.character)

class Level(object):
	def __init__(self):
		self.tiles=[]
		self.height=18
		self.width=70
		for y in range(1,self.height):
			zzz=[]
			for x in range(0,self.width):
				zzz.append(Tile("#"))
			self.tiles.append(zzz)
	def draw(self):
		y=2
		for row in self.tiles:
			x=0
			for col in row:
				col.draw(x,y)
				x+=1
			y+=1
	def randomy(self):
		return random.randint(0,self.height-1)
	def randomx(self):
		return random.randint(0,self.width-1)
	def room(self,x,y,w,h):
		if x<0: x=0
		if y<0: y=0
		if w+x>self.width: w=self.width-x-1
		if h+y>self.height: h=self.height-y-1
	
		for i in range(y,y+h):
			for o in range(x,x+w):
				self.tiles[i][o]=Tile(" ")

class DungeonLevel(Level):
	def __init__(self):
		Level.__init__(self)
		for i in range(0,random.randint(3,8)):
			self.room(self.randomx(),self.randomy(),random.randint(5,10),random.randint(5,8))

class Game:
	def __init__(self):
		toptext("Choose your class")
		character_class=choice_menu(["Spambot","Crawler"])
		s.clear()
		self.characters=[Player(text_prompt("Choose a nick"))]
		self.level=DungeonLevel()
		happening("Welcome to Ponppugue, {0}!".format(self.characters[0].nick))
		key_to_continue()

	def loop(self):
		s.clear()

		self.level.draw()
		for c in self.characters:
			c.draw()

		for c in self.characters:
			c.think()

def main(standard_screen):
	global s
	s=standard_screen
	toptext("Ponppu-roguelike v.0.0.1")
	key_to_continue()
	biginfo("""The forgotten domains of Internet. Once lit with the splendor of the never-ending dot-com-advertisements and flashy marquees, now a desolate ruin swept away in a corner. You are a program, a creation of an unknown programmer. A golem without a master, an ephemereal automaton without any purpose or function.

But there is a legend told amongst the IE-optimized web pages. A meme picture, the original, an uncropped version of a nude woman, a cat or perhaps something else? The one program to retrieve this sacred relic would surely ascend to a demigod status and transcend all migrations, from floppy to CD to SSD and beyond!

You set forth with your mission to find the relic or to be terminated in the attempt""")
	s.clear()
	game=Game()
	while True:
		game.loop()
	
curses.wrapper(main)

